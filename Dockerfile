FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/react-and-spring-data-rest-0.0.1-SNAPSHOT.jar
COPY ${JAR_FILE} react-and-spring-data-rest-0.0.1-SNAPSHOT.jar
ENTRYPOINT ["java","-jar","/react-and-spring-data-rest-0.0.1-SNAPSHOT.jar"]